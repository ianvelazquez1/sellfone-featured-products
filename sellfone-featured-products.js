import { html, LitElement } from 'lit-element';
import style from './sellfone-featured-products-styles.js';
import '@catsys/product-component/product-component.js';

class SellfoneFeaturedProducts extends LitElement {
  static get properties() {
    return {
     productAlign:{
       type:String,
       attribute:"product-align"
     },
     options:{
       type:Object
     }
    };
  }

  static get styles() {
    return style;
  }

  constructor() {
    super();
    this.productAlign="left";
    this.options={
      imageSrc:"",
      imageUrl:"",
      product1:{},
      product2:{}
    }
  }

  render() {
    return html`
        ${this.productAlign==="left"?html`
          <div class="gridLeft">
            <div class="imgContainer">
              <a href="${this.options.imageUrl}">
              <img class="featuredImg" src="${this.options.imageSrc}" alt="">
              </a>
             
            </div>
            <div class="item">
              <product-component @product-selected="${this.selectProduct}" class="product" .objectCell="${this.options.product1}"></product-component>
              </div>
              <div class="item">
              <product-component class="product" @product-selected="${this.selectProduct}" .objectCell="${this.options.product2}"></product-component>
              </div>
          </div>
        `:html`
        <div class="gridRight">
        <div class="item">
              <product-component class="product" @product-selected="${this.selectProduct}" .objectCell="${this.options.product1}"></product-component>
              </div>
              <div class="item">
              <product-component class="product" @product-selected="${this.selectProduct}" .objectCell="${this.options.product2}"></product-component>
              </div>
              <div class="imgContainer">
              <img class="featuredImg" src="${this.options.imageSrc}" alt="">
            </div>
          </div>
        `}
      `;
    }


    selectProduct(event){
       this.dispatchEvent(new CustomEvent("product-selected",{
         bubbles:false,
         composed:false,
         detail:event.detail
       }));
    }

}

window.customElements.define("sellfone-featured-products", SellfoneFeaturedProducts);
