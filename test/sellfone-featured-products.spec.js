/* eslint-disable no-unused-expressions */
import { fixture, assert } from "@open-wc/testing";

import "../sellfone-featured-products.js";

describe("Suite cases", () => {
  it("Case default", async () => {
    const _element = await fixture("<sellfone-featured-products></sellfone-featured-products>");
    assert.strictEqual(_element.hello, 'Hello World!');
  });
});
