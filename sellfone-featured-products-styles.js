import { css } from 'lit-element';

export default css`
:host {
  display: inline-block;
  box-sizing: border-box;
  width: 100%;
}

:host([hidden]), [hidden] {
  display: none !important;
}

*, *:before, *:after {
  box-sizing: inherit;
  font-family: inherit;
}

.gridLeft{
  display: grid;
  grid-template-columns: 50% 25% 25%;
}

.gridRight{
  display: grid;
  grid-template-columns: 25% 25% 50%;
}

.item{
  width: 100%;
  display: flex;
  justify-content: center;
  
}


.product{
  margin: 0 20px;
  --product-component-max-width:100%;
  --height-width:none;
  --product-component-min-width:200px;
  
}

.imgContainer{
  cursor: pointer;
  display: flex;
  align-items: center;

}

.featuredImg{
  max-width: 100%;
  object-fit: contain;
}`;
